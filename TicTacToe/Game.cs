﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Game
    {
        private char[,] _gameField = new char[,] { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        private int posX;
        private int posY;
        private char lastVal = ' ';
        private int turnsCount = 0;

        public Game()
        {
            posX = 0;
            posY = 2;
        }
    
        public bool IsOver()
        {
            if (Char.Equals(Combs(),'X') || Char.Equals(Combs(), 'O')|| Char.Equals(returnWinElement(), '#'))
            {
                return true;
            }
            return false;
        }

        public void goUp()
        {
            if (posY == 0) return;
            _gameField[posX, posY] = lastVal;
            lastVal = _gameField[posX, posY-1];
            _gameField[posX, posY - 1] = '^';
            posY--;
        }
        public void goDown()
        {
            if (posY == 2) return;
            _gameField[posX, posY] = lastVal;
            lastVal = _gameField[posX, posY +1];
            _gameField[posX, posY + 1] = '^';
            posY++;
        }
        public void goLeft()
        {
            if (posX == 0) return;
            _gameField[posX, posY] = lastVal;
            lastVal = _gameField[posX-1, posY];
            _gameField[posX-1, posY] = '^';
            posX--;
        }
        public void goRight()
        {
            if (posX == 2) return;
            _gameField[posX, posY] = lastVal;
            lastVal = _gameField[posX+1, posY];
            _gameField[posX+1, posY] = '^';
            posX++;
        }
        public void makeTurn()
        {
            if(lastVal!='O' && lastVal!='X' && turnsCount %2==0)
            {
                _gameField[posX, posY] = 'X';
                lastVal = 'X';
                turnsCount++;
            }
            else if (lastVal != 'X' && lastVal !='O' && turnsCount % 2 != 0)
            {
                _gameField[posX, posY] = 'O';
                lastVal = 'O';
                turnsCount++;
            }
        }
        public char Combs() // пока не придумал алгоритм - буду перебирать
        {
            for (int i = 0; i < _gameField.GetLength(0); i++)
            {
               if (Char.Equals(_gameField[i,0], _gameField[i, 1]) && Char.Equals(_gameField[i,1], _gameField[i, 2]) && !Char.Equals(_gameField[i, 1], ' '))
               {
                    return _gameField[posX, posY];
               }
               if (Char.Equals(_gameField[0, i], _gameField[1, i]) && Char.Equals(_gameField[1, i], _gameField[2, i]) && !Char.Equals(_gameField[1, i], ' '))
               {
                    return _gameField[posX, posY];
               }
            }
            if (Char.Equals(_gameField[0, 0], _gameField[1, 1]) && Char.Equals(_gameField[1, 1], _gameField[2, 2]) && !Char.Equals(_gameField[1, 1], ' '))
            {
                return _gameField[posX, posY];
            }
            if (Char.Equals(_gameField[0, 2], _gameField[1, 1]) && Char.Equals(_gameField[1, 1], _gameField[2, 0]) && !Char.Equals(_gameField[1, 1], ' '))
            {
                return _gameField[posX, posY];
            }
            return '-';
        }

        public char returnWinElement()
        {
            if (turnsCount == 9) return '#';
            return _gameField[posX, posY];
        }

        public void Print ()
        {
            Console.WriteLine("+------+------+------+");
            for (int i =0; i<_gameField.GetLength(0); i++)
            {
                Console.WriteLine("|      |      |      |");
                for (int j = 0; j < _gameField.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        Console.Write("|");
                    }
                    Console.Write("  {0,2}  ", _gameField[j, i]);
                    Console.Write("|");
                }
                Console.WriteLine("\n|      |      |      |");
                Console.WriteLine("+------+------+------+");
            }
        }
    }
}
