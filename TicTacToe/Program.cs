﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.Print();
            while(!game.IsOver())
            {
                Console.Clear();
                Console.WriteLine("Let's play the Tic-Tac-Toe game!");
                Console.WriteLine("Press <Enter> to make a turn, press <Up>, <Down>, <Left>, <Right> keys to navigate");
                game.Print();
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.UpArrow:
                        game.goUp();
                        break;
                    case ConsoleKey.DownArrow:
                        game.goDown();
                        break;
                    case ConsoleKey.LeftArrow:
                        game.goLeft();
                        break;
                    case ConsoleKey.RightArrow:
                        game.goRight();
                        break;
                    case ConsoleKey.Enter:
                        game.makeTurn();
                        break;
                }
            }
            if(game.IsOver())
            {
                Console.Clear();
                Console.WriteLine("Let's play the Tic-Tac-Toe game!");
                Console.WriteLine("Press <Enter> to make a turn, press <Up>, <Down>, <Left>, <Right> keys to navigate");
                game.Print();
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                if (Char.Equals(game.returnWinElement(), '#'))
                {
                    Console.WriteLine("There is no winner!");
                }
                else  Console.WriteLine($"The winner is {game.returnWinElement()}!");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
